package br.com.hs1.morada.processamento

import java.net.HttpURLConnection
import java.net.URL

fun notificar() {
//        val url = URL("http://morada-rest-service/api/notificador")
        val url = URL(System.getenv("BACKEND_URL"))
        val con: HttpURLConnection = url.openConnection() as HttpURLConnection
        try {
            con.setRequestMethod("GET")
            con.setDoOutput(true)
            con.connect()
            println("RETORNO NOTIFICACAO ${con.responseCode}")
        } finally {
            con.disconnect();
        }
}
