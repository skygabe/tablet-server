data class Usuario(
        var id: Int?=null,
        var login: String?=null,
        var name: String?=null,
        var email: String?=null,
        var token: String?=null)