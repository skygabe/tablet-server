import br.com.hs1.morada.nucleo.checklist.Foto
import io.ebean.DB
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream

fun Route.foto(){
    post("foto") {
        val multipart = call.receiveMultipart()
        var idFoto : Int? = null
        multipart.forEachPart { part ->
            val bos = ByteArrayOutputStream()
            (part as PartData.FileItem).streamProvider()
                .use { input -> bos.buffered().use { output -> input.copyToSuspend(output) } }
                    Foto().apply {
                        imagem = bos.toByteArray()
                        save()
                        idFoto = id
            }
            part.dispose()

        }
        call.respond(HttpStatusCode.OK, idFoto!!)


        //O CODIGO ABAIXO TRAVA DEPOIS DA 2.A FOTO
//        val read = call.receiveMultipart().readPart()
//        val part = read as PartData.FileItem
//        val foto = part.streamProvider().use {
//            Foto().apply {
//                val bos = ByteArrayOutputStream()
//                it.copyToSuspend(bos)
//                imagem = bos.toByteArray()
//                save()
//                println("salvou")
//            }
//        }
//        part.dispose()
//        println("antes de responder")
//        call.respond(HttpStatusCode.OK, foto.id?:0)
//        println("depois de responder")
    }
}


suspend fun InputStream.copyToSuspend(
    out: OutputStream,
    bufferSize: Int = DEFAULT_BUFFER_SIZE,
    yieldSize: Int = 4 * 1024 * 1024,
    dispatcher: CoroutineDispatcher = Dispatchers.IO
): Long {
    return withContext(dispatcher) {
        val buffer = ByteArray(bufferSize)
        var bytesCopied = 0L
        var bytesAfterYield = 0L
        while (true) {
            val bytes = read(buffer).takeIf { it >= 0 } ?: break
            out.write(buffer, 0, bytes)
            if (bytesAfterYield >= yieldSize) {
                yield()
                bytesAfterYield %= yieldSize
            }
            bytesCopied += bytes
            bytesAfterYield += bytes
        }
        return@withContext bytesCopied
    }
}