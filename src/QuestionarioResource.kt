import br.com.hs1.morada.nucleo.checklist.CheckList
import br.com.hs1.morada.nucleo.checklist.query.QCheckList
import br.com.hs1.morada.nucleo.checklist.query.QFoto
import br.com.hs1.morada.processamento.notificar
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.questionario(){
    post("questionario") {
        val numSerie = call.parameters.get("serial")?:throw Exception("Número de série não informado")
        val idOperador = call.parameters.get("operador")?.toInt()?:throw Exception("Id do operador não informado")
        val dto = call.receive<CheckList>()
        QuestionarioService.gravar(dto, numSerie, idOperador)
        call.respond(HttpStatusCode.OK)
        notificar()
    }
}

fun main(){
    val qf = QFoto()
    val chec =  QCheckList()
        .respostas.fetch()
        .respostas.fotos.fetch()
        .order().id.desc().setMaxRows(1).findOne()
    println(chec?.toJSON())
}