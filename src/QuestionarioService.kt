
import br.com.hs1.morada.nucleo.checklist.CheckList
import br.com.hs1.morada.nucleo.checklist.query.QFoto
import br.com.hs1.morada.nucleo.prensa.query.QMunicipio
import br.com.hs1.morada.nucleo.seguranca.query.QPessoa
import io.ebean.DB

object QuestionarioService {
    fun gravar(dto: CheckList, numSerie: String, idOperador: Int){
        dto.apply {
            println(toJSON())
            veiculo = getVeiculoBySerial(numSerie)
            municipio = getMunicipioByCoord(dto.lat, dto.lon)
            operador = QPessoa().id.eq(idOperador).findOne()
            DB.beginTransaction().use {transaction ->
                save()
                respostas?.forEach { resp ->
                    val ids = resp.fotos?.map { it.idServer }
                    if(ids != null && ids.isNotEmpty()) {
                        //aponta as fotos previamente enviadas para as respostas
                        QFoto().id.`in`(ids).asUpdate()
                            .set("resposta_id", resp.id)
                            .update()
                    }
                }
                transaction.commit()
            }
        }
    }
}