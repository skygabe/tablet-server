import br.com.hs1.morada.nucleo.checklist.query.QCheckList
import br.com.hs1.morada.nucleo.telemetria.Jornada
import br.com.hs1.morada.processamento.notificar
import io.ebean.DB
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.jornada(){
    post("jornada") {
        val numSerie = call.parameters.get("serial")?:throw Exception("Número de série não informado")
        val dto = call.receive<Jornada>()
        JornadaService.gravar(dto, numSerie)
        notificar()
        call.respond(HttpStatusCode.OK)
    }
}

fun main(){
//    val jorn = QAtividadeExecutada().setMaxRows(5).findList()
    val jorn = QCheckList()
        .respostas.fetch()
        .setMaxRows(1).findOne()
    println(DB.json().toJson(jorn))
}
