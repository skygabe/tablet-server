package br.gov.prf.ecomultas.ecobackend.api

import Usuario
import br.com.hs1.morada.nucleo.seguranca.Autorizacao
import br.com.hs1.morada.nucleo.seguranca.query.QAutorizacao
import br.com.hs1.morada.nucleo.seguranca.query.QPessoa
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.call
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.config.*
import io.ktor.features.origin
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.receive
import io.ktor.response.*
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.routing.route
import java.util.*

fun Route.auth(env: Enviroment){

    route("auth") {
        post("login") {
            val credencial = call.receive<Credencial>()
            val p =  QPessoa().login.eq(credencial.login).senha.eq(credencial.password).findOne()
            if(p == null) {
                call.respond(HttpStatusCode.Unauthorized)
            } else {
                val lista = QAutorizacao().grupo.pessoas.equalTo(p).findList()
                    .groupBy { it.transacao }.map {
                        val aut = Autorizacao()
                        aut.transacao = it.key
                        it.value.forEach { aut.addPermissoes(it.permissoes?:"") }
                        aut.toString()
                    }
                println(p)
                val ip = call.request.origin.remoteHost
                val token = env.createToken(lista)
                call.respond(Usuario(p.id, credencial.login, p.nome, p.email, token))
            }
        }
    }
}


fun Authentication.Configuration.jwt( env: Enviroment) {
    jwt {
        verifier(env.jwtVerifier)
        realm = env.realm
        validate {
            val origin = this.request.origin
            val params = this.parameters
            val headers = this.request.headers
            object: Principal{

            }

//            UsuarioJwt(
//                it.payload.getClaim("id").asInt(),
//                it.payload.getClaim("cpf").asString(),
//                it.payload.getClaim("nome").asString(),
//                it.payload.getClaim("uf").asString(),
//                it.payload.getClaim("matricula").asString(),
//                it.payload.getClaim("lotacao").asString(),
//                it.payload.getClaim("ufLotacao").asString(),
//                it.payload.getClaim("nomeGuerra").asString(),
//                Funcionalidades(
//                    it.payload.getClaim("controlePermissoes").asArray(String::class.java)
//                )
//            )
        }
    }
}

class Enviroment(val config: ApplicationConfig){
    val realm = config.property("jwt.realm").getString()
    val issuer = config.property("jwt.issuer").getString()
    val secret = config.property("jwt.secret").getString()
    val duration = config.property("jwt.validity_ms").getString().toInt()

    val algorithm = Algorithm.HMAC512(secret)

    val jwtVerifier = JWT
        .require(algorithm)
        .withIssuer(issuer)
        .build()

    fun createToken(items: List<String>) =  JWT
        .create()
        .withIssuer(issuer)
        .withArrayClaim("groups", items.toTypedArray())
        .withIssuedAt(Date())
        .withExpiresAt(Date(Date().time + duration))
        .sign(algorithm)

}

data class Credencial(val login: String, val password: String)