package com.example

import Teste
import br.gov.prf.ecomultas.ecobackend.api.*
import com.kumuluz.ee.rest.beans.QueryParameters
import com.kumuluz.ee.rest.utils.JPAUtils
import foto
import getPerguntas
import getVersao
import getVersaoCheckList
import io.ebean.DB
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.features.*
import org.slf4j.event.*
import io.ktor.auth.*
import io.ktor.jackson.*
import io.ktor.server.cio.*
//import io.ktor.server.jetty.*
//import io.ktor.server.tomcat.*
import io.ktor.server.engine.*
import io.ktor.util.*
import jornada
import listagens
import questionario
import respondJson
import java.text.DateFormat
import java.util.*

fun main(args: Array<String>): Unit = EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    val env = Enviroment(environment.config)

    install(Locations) {
    }

    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(DataConversion)

    install(Authentication) {
        jwt( env)
    }

    install(ContentNegotiation) {
        jackson {

        }
    }

    routing {
        auth(env)

        get("/ambiente"){
            call.respondText("DEV")
        }

        get("/json/gson") {

            call.respond(Teste("kiko"))
        }
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        authenticate {
            get("tablet/versao"){
                call.respond(getVersaoCheckList())
            }
            get("pergunta"){
              call.respond(getPerguntas())
            }
            questionario()
            foto()
            jornada()
            listagens()
//            get<MyLocation> {
//                call.respondText("Location: name=${it.name}, arg1=${it.arg1}, arg2=${it.arg2}")
//            }
//            // Register nested routes
//            get<Type.Edit> {
//                call.respondText("Inside $it")
//            }
//            get<Type.List> {
//                call.respondText("Inside $it")
//            }
        }

        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }
        }


    }
}

class AuthenticationException : RuntimeException()
class AuthorizationException(message:String) : RuntimeException(message)
