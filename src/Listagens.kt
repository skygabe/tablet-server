import io.ebean.DB
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*

val jsonType = ContentType("application","json")

fun Route.listagens(){
    get<EndPoint> {
        val disp = getDispositivo(it.serial)
        if(disp == null) {
            call.respond(HttpStatusCode.Unauthorized, "Equipamento ${it.serial} não autorizado")
        } else {
            val lista : Iterable<Any> = when (it.name) {
                "fornecedor" -> getFornecedores(it.versao)
                "municipio" -> getMunicipios(it.versao)
                "atividadeMotorista" -> getAtividadesMotorista(it.versao)
                "checklist" -> getItensCheckList(it.versao)
                "componente" -> getComponentes(it.versao)
                "material" -> getMateriais(it.versao)

                "tipo_manutencao" -> getTipoManutencao(it.versao)
                else -> {
                    call.respond(HttpStatusCode.NotFound)
                    return@get
                }
            }
            call.respondJson(lista)
        }
    }
}

@Location("/{name}/list") data class EndPoint(val name: String,val serial: String, val versao: Long )

suspend inline fun ApplicationCall.respondJson(message: Any) {
    response.call.respondTextWriter(jsonType){
        this.write(DB.json().toJson(message))
    }
}

