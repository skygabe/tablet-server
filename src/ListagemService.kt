import br.com.hs1.morada.nucleo.checklist.query.QPergunta
import br.com.hs1.morada.nucleo.prensa.Municipio
import br.com.hs1.morada.nucleo.prensa.Veiculo
import br.com.hs1.morada.nucleo.prensa.query.*
import br.com.hs1.morada.nucleo.query.QVersao
import br.com.hs1.morada.nucleo.query.QVersaoCheckList
import br.com.hs1.morada.nucleo.seguranca.query.QPessoa
import java.util.*

fun getMunicipios(versao: Long) : List<Municipio> {
    val q= QMunicipio()
    return q.select(q.nome, q.uf).setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()
}

fun getFornecedores(versao: Long) = QFornecedor().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()
fun getAtividadesMotorista(versao: Long) =
    QAtividadeMotorista().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()

fun getItensCheckList(versao: Long) = QItemCheckList().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()

fun getComponentes(versao: Long) = QComponente().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()

fun getMateriais(versao: Long) = QMaterial().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()

fun getMotoristas(versao: Long) = QPessoa().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()

fun getPerguntas() = QPergunta().setIncludeSoftDeletes().findList()

fun getTipoManutencao(versao: Long) =
    QTipoManutencao().setIncludeSoftDeletes().ultimaAtualizacao.after(Date(versao)).findList()

fun getVersaoCheckList() =  QVersaoCheckList().findList()

fun getVersao(versao: Long) = QVersao().versao.gt(Date(versao)).findList()

fun getDispositivo(numSerie: String) = QDispositivo().numSerie.eq(numSerie).findOne()?: throw Exception("Dispositivo serial $numSerie não encontrado")
fun getMunicipioByCoord(lat: Double?, lng: Double?) =
        QMunicipio()
    .select("id")
    .raw("st_within(ST_SetSRID(st_point(?, ?), 4326), geom)",lng,lat)
    .findOne()
fun getVeiculoBySerial(numSerie: String): Veiculo {
    val disp = getDispositivo(numSerie)
    return disp.veiculo ?: throw Exception("Nenhum veículo associado ao tablet $numSerie")
}