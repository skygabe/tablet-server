
import br.com.hs1.morada.nucleo.telemetria.Jornada
import br.com.hs1.morada.nucleo.telemetria.query.QJornada

object JornadaService {
    fun gravar(dto: Jornada, numSerie: String){
        val veic = getVeiculoBySerial(numSerie)
        val q = QJornada()
        val jornada = q.select(q.id)
            .veiculo.equalTo(veic)
            .and().idJornada.eq(dto.idJornada)
            .and().codigoData.eq(dto.codigoData).findOne()
        if(jornada == null) {
            dto.veiculo = veic
            dto.save()
        }
        else {
            dto.id = jornada.id
            dto.update()
        }
    }
}
