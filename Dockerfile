FROM adoptopenjdk/openjdk8-openj9:alpine-jre
#FROM adoptopenjdk/openjdk11-openj9:alpine-jre
#FROM adoptopenjdk/openjdk8-openj9:alpine-slim
COPY build/libs/tablet-server*.jar /app/app.jar
#CMD ["java", "-jar", "/app/app.jar"]
CMD ["java", "-Xshareclasses:cacheDir=/opt/shareclasses", "-jar", "/app/app.jar"]